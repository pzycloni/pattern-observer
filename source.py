import sympy
from abc import ABCMeta, abstractmethod

# interface of observable
class Observable(metaclass=ABCMeta):
	@abstractmethod
	def subscribe(self, event):
		pass

	@abstractmethod
	def rss(self, number):
		pass

class Observable:
 	def __init__(self):
 		self.events = list()

 	def subscribe(self, event):
 		self.events.append(event)

 	def rss(self, number):
 		for event in self.events:
 			event.update(number)


class NumberObservable(Observable):
	def scan(self, number):
		self.rss(number)

# interface of listener
class Subscriber(metaclass=ABCMeta):
	@abstractmethod
	def update(self, message):
		pass


class EvenSubscriber(Subscriber):
	def update(self, message):
		if not number % 2:
			print("Число четное")


class NotEvenSubscriber(Subscriber):
	def update(self, message): 
		if number % 2:
			print("Число нечетное")


class ModThreeSubscriber(Subscriber):
	def update(self, message):
		if not number % 3:
			print("Число кратное 3-м")


class MoreThanZeroSubscriber(Subscriber):
	def update(self, message):
		if number > 0:
			print("Число больше 0")


class LessThanZeroSubscriber(Subscriber):
	def update(self, message):
		if number < 0:
			print("Число меньше 0")


class SimpleNumberSubscriber(Subscriber):
	def update(self, message):
		if sympy.isprime(number):
			print("Простое число")



if __name__ == "__main__":
	number = int(input("Введите число: "))

	numberObs = NumberObservable()

	numberObs.subscribe(EvenSubscriber())
	numberObs.subscribe(NotEvenSubscriber())
	numberObs.subscribe(ModThreeSubscriber())
	numberObs.subscribe(MoreThanZeroSubscriber())
	numberObs.subscribe(LessThanZeroSubscriber())
	numberObs.subscribe(SimpleNumberSubscriber())

	numberObs.scan(number)